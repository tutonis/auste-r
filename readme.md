```
STATISTIKA 2019	 RUDUO
```
### DOC.	DR.	JURGITA	MARKEVIČIŪTĖ,	DOC.	DR.	GEDIMINAS MURAUSKAS 1	

# Statistika

# Bendrieji	reikalavimai

1. Laboratorinių	darbų	atsiskaitymai	vyks	pratybų	metu.
2. Atsiskaitymų	datos	paskelbtos	VMA.
3. Neatvykus	į	atsiskaitymą	be	pateisinamos	priežasties,	kitas	atsiskaitymo	laikas	nebus	skiriamas.
4. Neatvykus	į	atsiskaitymą	su	pateisinama	priežastimi	(gydytojo	pažyma),	atsiskaityti	bus	galima	
    paskutinę	semestro	pratybų	paskaitą.

## 1 - asis	laboratorinis	darbas

1. Atsiskaitymo	metu	studentas	gauna	10	užduočių	(klausimų)	iš	duomenų	įvedimo,	tvarkymo	ir	
    analizės	su	R	programa.
2. Užduočių	pavyzdžiai	yra	pateikti	VMA	aplinkoje.
3. Studentas	atsiskaitymo	metu	turi	žinoti	ir	mokėti	pritaikyti	komandas,	aprašytas	failuose		“R	
    duomenų	struktūros”,	R	duomenų	importavimas”,		“R	duomenų	tvarkymas”, “R	grafikai”.
4. R	scenarijai	su	sprendimais	turi	būti	įkelti	į	VMA	nurodytu	laiku.

## 2 - asis	laboratorinis	darbas

1. Parsisiųskite	nurodytus	duomenis	(duomenų	rinkmena	toliau	vadinama	„apklausa“).	Sudarykite	
    duomenų	rinkinį	(parašyti	R	kodą),	kuriame	būtų	nemažiau	penkių	stulpelių	(kintamųjų).	Bent	
    vienas	kintamasis	turi	būti	kategorinis,	bei	bent	vienas	intervalinis.	
2. Parsisiųskite	ne	mažiau	kaip	15-os paskutiniųjų	metų	nurodytų	rodiklių	duomenis	iš	pasirinktų	
    šaltinių	(Eurostat,	Pasaulio	banko,	Tarptautinio	valiutos	fondo,	Lietuvos	statistikos	departamento ir	
    t.t.). Sudarykite	duomenų	rinkinį	(parašyti	R	kodą), kuriame	būtų	laiko	stulpelis	(METAI)	ir	rodiklių
    stulpeliai (duomenų	rinkmena	toliau	vadinama	„macro“).
3. Rinkmenoje	„apklausa“	
    a. Kategoriniams	kintamiesiems sudarykite	dažnių	bei	santykinių	dažnių	lenteles	ir	išbrėžkite	
       stulpelių	diagramą.
    b. Intervaliniams	kintamiesiems	apskaičiuokite	padėties,	sklaidos	bei	formos	skaitines	
       charakteristikas	pagal	kategorinio	kintamojo(-ųjų)	pjūvį(-ius).	
    c. Išbrėžkite	pasirinktų	rodiklių	stačiakampę	diagramą	bei	histogramą	pagal	kategorinio	
       kintamojo(-ųjų)	pjūvį(-ius).
4. Apibūdinkite	„macro“		kintamuosius;	apskaičiuokite	padėties,	sklaidos	bei	formos	skaitines	
    charakteristikas; pateikite	gautų	charakteristikų	interpretaciją	bei	ekonominį	pagrindimą;	Išbrėžkite	
    pasirinktų	rodiklių	linijos	grafiką	bei	histogramą.
5. Rinkmenoje	„apklausa“	pasirinkite	du	kintamuosius	ir	atlikite	t-testą.	(Nepamirškite	parašyti	tyrimo	
    hipotezę,	statistinę	hipotezę	bei	tyrimo	išvadą).	
6. Rinkmenoje	„apklausa“	pasirinkite	intervalinį kintamajį bei	suskaičiuokite	vidurkio	ir	dispersijos	
    taškinius	įverčius	bei	vidurkio	pasikliautinąjį intervalą.	
7. 6 - oje	dalyje	gautus	taškinius	įverčius	panaudokite	atsitiktinių	normaliųjų	dydžių	generavimui.	Porai	
    (vidurkis,	dispersija)	sugeneruokite	100	normaliųjų atsitiktinių	dydžių	imčių	(imties	dydis	500	stebėjimų).
    Apskaičiuokite	gautų	imčių	taškinius	vidurkio	bei	dispersijos	įverčius. Gautiems	vidurkių	
    taškiniams	įverčiams	išbrėžkite	histogramą	bei	stačiakampę	diagramą.	Ką	galite	pasakyti	apie	
    įverčių	stabilumą?
8. Rinkmenos	„macro“	kintamiesiems išskirkite	tiesinius trendus. Pavaizduokite	duomenų	ir	trendų	
    grafikus. Apibūdinkite	laiko	eilučių	tendencijas.
9. Pasirinkite kelis arba	visus „macro“	duomenų	kintamuosius	ir	 sudarykite	tiesinę	regresiją ir	atlikite	
    prognozę. Interpretuokite	gautus	rezultatus


Antro	laboratorinio	atsiskaitymui studentas	privalo	iki	gruodžio	5	d.	23	val.	įkelti	į	VMA	trumpą	
ataskaitą	apie	savo	atliktą	užduotį (pdf	formatu).	Ataskaitos	pavadinimas -- studento	pažymėjimo	
numeris,	(pvz.	s1234567). Ataskaitoje	turi būtų	šios	dalys:
a. Užduotį	atlikusio	studento	vardas,	pavardė,	grupė;
b. Duomenų	rinkinio	numeris;
c. Užduotis;
d. R	kodas;
e. R	pateikti	atsakymai	bei	grafikai	(angl.	R	output);
f. Trumpas	tekstas	su	atsakymais	į	užduotus	klausimus	bei	pateiktų	rezultatų	analizė (ne	
mažiau	200	žodžių).

