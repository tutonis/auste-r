install.packages("readxl")
install.packages("summarytools")
install.packages("ggplot2")
install.packages("dplyr")
install.packages("psych")

#Aktyvinau readxl biblioteka
library("readxl")

#Duomenu nuskaitymas i r

#NUORODYT DUOMENU FOLDER
#setwd("C:/Users/Agota/Desktop/r")
#"apklausa" - 5 kintamieji
duom<-read.csv2("apklausa.csv", header = TRUE, sep=",", dec = ".")
columns<-c("PL150","PY010G","PL060","PL080","PY120G")

apklausa<-duom[columns]
head(apklausa)

print(columns)

#macro duomenys
macro<- read_excel("macro.xlsm")

#3 uzduotis
#a 
#dazniu lenteles sudarymas

library(summarytools)
summarytools::freq(apklausa$PL150, order="freq")
#diagramos braizymas

library("ggplot2")

p <- ggplot(apklausa, aes(PL150)) + 
  geom_bar(aes(y = (..count..)/sum(..count..))) + 
  scale_y_continuous(labels=scales::percent) +
  labs(x="Ar turite pavaldiniu?",y="Procentai")+
  ggtitle("Pavalidnius turinciu asmenu (procentai)")+
  theme(axis.text.x = element_text(face="bold",  size=8, angle=90))

p

#b

library(dplyr)

#Penkiaskaite suvestine
dm<-select(apklausa, "PY010G", "PY120G") 
summary(dm)

#    Papildomos sklaidos  charakteristikos
summarise_all(dm,list(StdNuok = sd, Dispersija = var, MAD = mad),na.rm=T)

# papildomos charakteristikos (asimetrijos koef. ir ekscesas) # 
library(psych) 
describe(dm,na.rm=T)

#  Charakteristikos (pjuvis -- PL150 ar turit pavaldiniu (taip-ne)) #
dm<-select(apklausa, PL150, PY010G,PY120G) 
dm %>% group_by(PL150)%>% summarise_all(list(StdNuok = sd, Dispersija = var),na.rm=T)


#c
library("ggplot2")
p <- ggplot(apklausa, aes(x=PL150, y=PY010G,color=as.factor(PL150))) +  labs(x="Turi,neturi",y="pajamos",color="pavaldiniai")+  
  geom_boxplot()+ggtitle("Staciakampe diagrama")+  scale_x_discrete(labels = c('Turi','Neturi')) 
p 


# Paprasta histograma # 
ggplot(data=apklausa, aes(x=PY010G)) +   
  geom_histogram()


# Histograma, pasirinkus grupavimo intervalo ilgi 100 # 
ggplot(data=apklausa, aes(x=PY120G)) +   
  geom_histogram(binwidth=100)+  
  scale_x_continuous(breaks=seq(0,4500, 500),labels = seq(0,4500, 500))+  
  labs(x="Pajamos",y="Daznis")+  ggtitle("Pajamu histograma")

# Histograma PY120G(pajamos) pagal pjuvi PL150(turi-neturi) # 
apklausa$PL150<-as.factor(apklausa$PL150) 
levels(apklausa$PL150)<-c("Turi","Neturi") 
ggplot(apklausa, aes(x=PY120G, group=PL150, color=PL150)) +  
  geom_histogram(fill="white",binwidth=200, alpha=0.5, position="identity")+  
  labs(x="Pajamos",y="Daznis",group="pavaldiniai",color="pavaldiniai")+ 
  ggtitle("Pajamu histograma")


#4
#   charakteristikos # 
#  Penkiaskaite sistema # 
summary(macro[,-1])

#    Papildomos sklaidos  charakteristikos # 
sklaidosCh<-summarise_all(macro[,-1],list(StdNuok = sd, Dispersija = var))
sklaidosCh

# papildomos charakteristikos #
library(psych) 
describe(macro[,-1],na.rm=T)

#  liniju grafikas 
p <- ggplot() +   
  geom_line(data = macro, aes(x = Metai, y = kintamasis2), color = "blue") +
  scale_x_continuous(breaks=macro$Metai,labels = macro$Metai)+  
  geom_point()+  
  theme(axis.text.x = element_text(face="bold",  size=8, angle=90))+  
  ggtitle("Gimusiu moteru skaicius") 
p

#  histograma, pasirinkus grupavimo intervalo ilgi #
ggplot(data=macro, aes(x=kintamasis2)) +   
  geom_histogram(binwidth=5000)+ 
  ylab("Daznis")+ 
  ggtitle("Gimusiu moteru skaicius histograma")


#5

#duomenu isgrupavimas
turi<-duom%>%select(PY010G, PL150)%>%filter(PL150 == 1)
neturi<-duom%>%select(PY010G, PL150)%>%filter(PL150 == 2)


var.test(turi$PY010G, neturi$PY010G)

#APRASYTI
t.test(turi$PY010G, neturi$PY010G, paired = FALSE, var.equal = FALSE)


#6


Kint<-select(apklausa, "PY010G")

#Taskiniai iverciai
rezK1<-summarise(Kint, vidurkis = mean(PY010G, na.rm=TRUE), dispersija=var(PY010G, na.rm=TRUE), standnuokrypis=sd(PY010G, na.rm=TRUE))
rezK1

#pasikliautinis intervalas
errorK1<- qt(0.975, df=length(Kint$"PY010G")-1)*sd(Kint$"PY010G", na.rm=TRUE)/sqrt(length(Kint$"PY010G"))

leftK1<-mean(Kint$PY010G, na.rm=TRUE)-errorK1
rightK1<-mean(Kint$PY010G, na.rm=TRUE)+errorK1

leftK1
rightK1
#Paiskliautinasis intervalas [10649.13 , 11265.32]


#7

# generavimas vektoriu K1 kintamajam
library(ggplot2)

#vektorius vidurkiu reiksmem
sample_means<-rep(NA, 100)
#vektorius dispersijos reiksmem
sample_var<-rep(NA, 100)


# priskiriamas vidurkis ir dispersija
for(i in 1:100){
  sample_means[i] <- mean(rnorm(500, mean = rezK1$vidurkis, sd = rezK1$standnuokrypis))
  sample_var[i] <- mean(rnorm(500, mean = rezK1$dispersija, sd = rezK1$standnuokrypis))
}

# vidurkiu histograma
hist(sample_means, main = "Vidurkiu histograma", xlab="Vidurkiai", ylab
     = "Duomenys", las=1, col="pink")
# dispersijos histograma
hist(sample_var, main = "Dispersiju histograma", xlab="Dispersijos", ylab
     = "Duomenys", las=1, col="pink")


#vidurkiu staciakampe diagrama
library(ggplot2)
boxplot(sample_means, xlab="Vidurkiai", ylab="Duomenys", main="vidurkiu staciakampe
diagrama", col="red")
#dispersijos staciakampe diagrama
library(ggplot2)
boxplot(sample_var, xlab="dispersijos", ylab="Duomenys", main="dispersiju staciakampe
diagrama", col="red")


# kiek kartu samdomo darbo pajamu vidurkis pakliuvo i pasikliautinaji intervala
sm<-0
observations<-matrix(rnorm(50000, mean=rezK1$vidurkis, sd=rezK1$standnuokrypis), 500, 100)
meansK1<-apply(observations, 2, mean)
deviationK1<-apply(observations, 2, sd)
CIU_simK1<-meansK1+qt(0.975, df = 500-1)*deviationK1*sqrt(1/500)
CIU_simK1

#8

# trendo funkcija
###########################################
trnd<-lm(kintamasis1~Metai, data=macro)
summary(trnd)
# grafikas
library(ggplot2)
ggplot(macro, aes(Metai, kintamasis1)) +
  scale_x_continuous(breaks=macro$Metai,labels=macro$Metai)+
  geom_line(stat="identity") +
  geom_point(size=1) +
  stat_smooth(method = "lm", se = FALSE)



#9
# Koreliacija
cor(macro[,-1])

#Koreliacijos testas
library("psych")
cr <- corr.test(macro[,-1], ci=TRUE)
cr
cr$r
cr$p
cr$ci.adj

#Regresija
regr<-lm(kintamasis1~kintamasis2+kintamasis3+kintamasis4,data=macro)
summary(regr)
#install.packages("DescTools")
library(DescTools)
DescTools::VIF(regr)

regr2<-lm(kintamasis1~kintamasis2+kintamasis3,data=macro)
summary(regr2)
DescTools::VIF(regr2)

regr3<-lm(kintamasis1~kintamasis2,data=macro)
summary(regr3)

#Regresijos grafikas

#Prognoze
predicted_macro <- data.frame(mpg_pred = predict(regr3, macro), kintamasis3=macro$kintamasis3)

# tiesine regresijos grafikas
ggplot(data = macro, aes(x = kintamasis1, y = kintamasis3)) + 
  geom_point(color='blue') +
  geom_line(color='red',data = predicted_macro, aes(x=mpg_pred, y=kintamasis3))



